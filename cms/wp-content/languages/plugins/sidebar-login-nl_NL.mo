��    !      $  /   ,      �  5   �  X     ]   x     �  O   �  %   7     ]     c     s     �     �     �     �     �     �     �     �     �  1        I     R     ^     z     �     �     �     �     �     �     �       2   %  $  X  @   }  a   �  [         |  Q   �  &   �     	     	     	     +	     4	     =	     Q	     f	     {	  
   �	     �	     �	  8   �	     �	     
      
     9
      Y
     z
     �
     �
     �
     �
     �
     �
  2                           	                                                          
                          !                                                   <a href="%s">Anyone can register</a> must be enabled. <a href="%s">Capability</a> (optional) refers to the type of user who can view the link. Allows you to easily add an ajax-enhanced login widget to the sidebar on your WordPress site. Current page URL Dashboard | %admin_url%
Profile | %admin_url%/profile.php
Logout | %logout_url% Displays a login area in the sidebar. Links Logged-in title Logged-out title Login Login &rarr; Login Redirect URL Logout Redirect URL Lost Password Mike Jolley Password Please enter your password Please enter your username Please enter your username and password to login. Register Remember Me Show "Remember me" checkbox Show logged-in user avatar Show lost password link Show register link Sidebar Login Text | HREF Text | HREF | Capability Username Welcome %username% http://mikejolley.com http://wordpress.org/extend/plugins/sidebar-login/ PO-Revision-Date: 2017-06-15 09:32:13+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: nl
Project-Id-Version: Plugins - Sidebar Login - Stable (latest release)
 <a href="%s">Iedereen kan registreren</a> moet ingeschaked zijn. <a href="%s">Zichtbaarheid</a> (optioneel) verwijst naar het type gebruiker die de link kan zien. Hiermee kun je een ajax-enhanced login widget in de sidebar van je WordPress site plaatsen. URL huidige pagina Dashboard | %admin_url%
Profiel | %admin_url%/profile.php
Uitloggen| %logout_url% Toont een log-in scherm in de sidebar. Links Log-in titel Uitgelogd titel Inloggen Inloggen Log-in redirect URL Log-uit redirect URL Wachtwoord vergeten? Mike Jolley Wachtwoord Voer uw wachtwoord in Voer uw gebruikersnaam in Voer uw gebruikersnaam en wachtwoord in om in te loggen. Registreren Ingelogd blijven Toon "Ingelogd blijven" checkbox Toon de avatar van de gebruiker Toon 'Wachtwoord vergeten?' link Toon 'Registreer' link Sidebar log-in Tekst | HREF Tekst | HREF | Zichtbaarheid Gebruikersnaam Welkom %username% http://mikejolley.com http://wordpress.org/extend/plugins/sidebar-login/ 